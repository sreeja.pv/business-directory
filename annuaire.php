<?php
/*
Plugin Name: Plugin Annuaire Connection 
Description: Create a plugin for contact directory
Author: Sreeja
Version: 1.0
*/

function shortcode_connection() {
  global $wpdb;
  $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}annuaire", OBJECT );

  echo "<table>
      <tr>
        <td></td>
        <td>Entreprise</td>
        <td>Localisation</td>
        <td>Prenom</td>
        <td>Nom</td>
        <td>Email_contact</td>
      </tr>";
  foreach($results as $entreprise){
    // echo "<div>
    echo "<tr><td>{$entreprise -> id }</td>
    <td>{$entreprise -> nom_entreprise}</td>
    <td>{$entreprise -> localisation_entreprise}</td>
    <td>{$entreprise -> prenom_contact}</td>
    <td>{$entreprise -> nom_contact}</td>
    <td>{$entreprise -> mail_contact}</td></tr>";
  }
  echo "</table>";
}

add_shortcode('connection', 'shortcode_connection');
